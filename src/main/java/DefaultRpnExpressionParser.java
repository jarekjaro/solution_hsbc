import resourceParsers.ParserNotFoundException;
import resourceParsers.RpnExpressionResourceParserFactory;
import resultWriters.RpnExpressionResultWriterFactory;

import java.util.List;
import java.util.Stack;

public class DefaultRpnExpressionParser extends AbstractRpnExpressionParser implements RpnExpressionParser {


    public DefaultRpnExpressionParser(
            RpnExpressionResourceParserFactory resourceParserFactory,
            RpnExpressionResultWriterFactory resultWriterFactory
    ) {
        super(resourceParserFactory, resultWriterFactory);
    }

    public Double parse(Object resource) throws ParserNotFoundException, RpnExpressionNotParsable {
        return compute(resourceParserFactory.getParserFor(resource).parse(resource));
    }

    private Double compute(List<String> rpnArgs) {
        Stack<Double> rpnStack = new Stack<>();

        for (String element : rpnArgs) {

            validateElement(element);

            if (isNumber(element))
                rpnStack.push(Double.valueOf(element));

            if (isOperator(element))
                makeOperation(rpnStack, element);
        }

        return rpnStack.pop();
    }

    private void validateElement(String element) {
        if (!isNumber(element) && !isOperator(element))
            throw new RpnExpressionNotParsable("Beacause of element '" + element + "' expression is not parsable.");
    }


    private boolean isNumber(String str) {
        try {
            Double.parseDouble(str);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    private boolean isOperator(String element) {
        return Operators.getOperators().contains(element);
    }

    private void makeOperation(Stack<Double> rpnStack, String element) {
        Double a = rpnStack.pop();
        Double b = rpnStack.pop();

        switch (element) {
            case Operators.PLUS:
                rpnStack.push(b + a);
                break;
            case Operators.MINUS:
                rpnStack.push(b - a);
                break;
            default:
                // would be used if operators strategy would be implemented
                throw new OperatorNotSupportedException(element);
        }
    }

}
