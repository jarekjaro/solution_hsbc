import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class Operators {

    public static final String PLUS = "+";

    public static final String MINUS = "-";

    private static final List<String> operators = Arrays.asList(PLUS, MINUS);

    public static List<String> getOperators() {
        return Collections.unmodifiableList(operators);
    }
}
