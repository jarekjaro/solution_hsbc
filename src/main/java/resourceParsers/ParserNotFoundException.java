package resourceParsers;

public class ParserNotFoundException extends RuntimeException {
    public ParserNotFoundException(String message) {
        super(message);
    }
}
