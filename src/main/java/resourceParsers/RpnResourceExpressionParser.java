package resourceParsers;

import java.util.List;

public interface RpnResourceExpressionParser<T> {

    List<String> parse(T resource);

}
