package resourceParsers;

public class RpnExpressionResourceParserFactory {

    public RpnResourceExpressionParser getParserFor(Object type) {
        if (type instanceof String[]) {
            return new StringArrayResourceExpressionParser();
        } else if (type instanceof String) {
            return new StringResourceExpressionParser();
        } else if (type != null) {
            throw new ParserNotFoundException("Parser not found for " + type.getClass().getSimpleName());
        } else {
            throw new ParserNotFoundException("Parser not found for 'null' type");
        }
    }
}
