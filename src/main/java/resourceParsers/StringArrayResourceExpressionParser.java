package resourceParsers;

import java.util.Arrays;
import java.util.List;

public class StringArrayResourceExpressionParser implements RpnResourceExpressionParser<String[]> {

    public List<String> parse(String[] resource) {

        return Arrays.asList(resource[0].split("\\s"));
    }
}
