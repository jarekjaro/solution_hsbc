package resourceParsers;

import java.util.Arrays;
import java.util.List;

public class StringResourceExpressionParser implements RpnResourceExpressionParser<String> {
    public List<String> parse(String resource) {
        return Arrays.asList(resource.split("\\s"));
    }
}
