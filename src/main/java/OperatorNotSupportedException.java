public class OperatorNotSupportedException extends RuntimeException {
    public OperatorNotSupportedException(String element) {
        super("Operator '" + element + "' is not yet supported.");
    }
}
