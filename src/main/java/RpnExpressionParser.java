import resourceParsers.ParserNotFoundException;

/**
 * Parser of Reversed Polish Notation expressions.
 */
public interface RpnExpressionParser {

    /**
     * Parses and computes the RPN expressions from given resource. The resource should be parsable to a string
     * representing the RPN expression with its elements separated by space. Example: "3 4 -"
     *
     * @param resource an object that can reasonably be the source of an expression
     * @return result of the expression parsing and computation
     * @throws ParserNotFoundException  when the parser for given resource type was not found
     * @throws RpnExpressionNotParsable when the parser cannot read the expression
     */
    Double parse(Object resource) throws ParserNotFoundException, RpnExpressionNotParsable;

}
