public class RpnExpressionNotParsable extends RuntimeException {
    public RpnExpressionNotParsable(String message) {
        super(message);
    }
}
