import resourceParsers.RpnExpressionResourceParserFactory;
import resultWriters.RpnExpressionResultWriterFactory;

public class AbstractRpnExpressionParser {

    protected RpnExpressionResourceParserFactory resourceParserFactory;

    protected RpnExpressionResultWriterFactory resultWriterFactory;

    public AbstractRpnExpressionParser(
            RpnExpressionResourceParserFactory resourceParserFactory,
            RpnExpressionResultWriterFactory resultWriterFactory
    ) {
        this.resourceParserFactory = resourceParserFactory;
        this.resultWriterFactory = resultWriterFactory;
    }
}
