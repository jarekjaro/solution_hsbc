import resourceParsers.RpnExpressionResourceParserFactory;
import resultWriters.RpnExpressionResultWriterFactory;

public class RPN_main {

    public static void main(String[] args) {

        Double result = new DefaultRpnExpressionParser(new RpnExpressionResourceParserFactory(), new RpnExpressionResultWriterFactory()).parse(args);

        System.out.println("Solution: " + result);
    }
}
