import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import resourceParsers.ParserNotFoundException;
import resourceParsers.RpnExpressionResourceParserFactory;
import resultWriters.RpnExpressionResultWriterFactory;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

@RunWith(value = MockitoJUnitRunner.class)
public class DefaultRpnExpressionParserTest {

    private DefaultRpnExpressionParser defaultRpnExpressionParser;

    @Before
    public void setUp() {
        defaultRpnExpressionParser = new DefaultRpnExpressionParser(
                new RpnExpressionResourceParserFactory(),
                new RpnExpressionResultWriterFactory()
        );
    }

    @Test
    public void shouldThrowRpnExpressionNotParsableIfWrongOperatorSpecified() {
        //given
        String expression = "3 4 *";

        //when
        try {
            defaultRpnExpressionParser.parse(expression);
            fail("Fail because exception was not thrown!");
        } catch (RpnExpressionNotParsable notParsable) {

            //then
            assertThat(notParsable.getMessage()).isEqualTo("Beacause of element '*' expression is not parsable.");
        }
    }

    @Test
    public void shouldThrowRpnExpressionNotParsableIfInputIsWrong() {
        //given
        String expression = "^34*-5";

        //when
        try {
            defaultRpnExpressionParser.parse(expression);
            fail("Fail because exception was not thrown!");
        } catch (RpnExpressionNotParsable notParsable) {

            //then
            assertThat(notParsable.getMessage()).isEqualTo("Beacause of element '^34*-5' expression is not parsable.");
        }
    }

    @Test
    public void shouldThrowParserNotFoundExceptionIfInputIsNull() {
        //given

        //when
        try {
            defaultRpnExpressionParser.parse(null);
            fail("Fail because exception was not thrown!");
        } catch (ParserNotFoundException notParsable) {

            //then
            assertThat(notParsable.getMessage()).isEqualTo("Parser not found for 'null' type");
        }
    }

    @Test
    public void shouldCalculateProperExpressionUsingMinusOperator() {
        //given
        String expression = "8 4 -";

        //when
        Double result = defaultRpnExpressionParser.parse(expression);

        //then
        assertThat(result).isEqualTo(4.0);
    }

    @Test
    public void shouldCalculateProperExpressionUsingPlusOperator() {
        //given
        String expression = "8 4 +";

        //when
        Double result = defaultRpnExpressionParser.parse(expression);

        //then
        assertThat(result).isEqualTo(12.0);
    }


}